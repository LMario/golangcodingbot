package main

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"

	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

// Textos do bot
var Textos = map[string]string{
	"gracioso":          "Você esta muito gracioso @",
	"regras":            "<b> Comandos(Usuarios):</b>\n\n/help\n/admins\n/regras\n/txt_da_vergonha\n\n<b> Comandos(Admins):</b>\n\n/func_regras\n/kick\n/ban\n/clear\n",
	"mural_da_vergonha": "\nParabens!\nSeu nome esta no txt da vergonha\n",
	"welcome":           "<b>Eai!! seja bem vindo a alcateia! Mas conta ai , como chegou aqui ?</b>",
	"apagado":           "foi apagado com sucesso!",
	"user_not_found":    "O usuario não consta em nossa base de dados!",
	"removido_por":      "Usuário removido pelo ADM",
	"te_peguei":         "<b>Te peguei com a boca na butija ne?\nParabens!\nVocê estava tentando limpar seu nome do Muralzinho...MAIS NAÃO VAI!</b>",
}

//VerificaComando vê se a mensagem é um comando ou não
func VerificaComando(mensagem string) bool {
	if string(mensagem[0]) == "/" {
		return true
	}
	return false
}

func funcRegras(mensagem string, username string, idUsuario int) string {
	novasRegras := strings.Replace(mensagem, "/func_regras", "", -1) //Tira o comando da mensagem  :D
	userAdmin = permCheck(idUsuario)
	if userAdmin == "false" {
		txtdavergonhaArquivo(username, idUsuario, "alterar as regras")
		return Textos["gracioso"] + username + Textos["mural_da_vergonha"]
	}
	escreveArquivo(Rules, novasRegras)
	log.Printf("[-]Regras atualizadas por " + userAdmin)
	return "<b>As regras foram atualizadas com sucesso pelo admin " + userAdmin + "!</b>"

}

func regrasPrint() string {
	regras := leitorArquivo(Rules)
	return regras + ""
}
func help() string {
	return Textos["regras"]
}
func adminsComando(ChatID int64, bot *tgbotapi.BotAPI) string {
	return "<b>Caso ocorra algum problema ,não fale com sua mãe, fale com : \n\n@janmesu (Mantainer Atual)</b>"
}
func kickComando(ChatID int64, mensagem string, idUsuario int, username string, db *sql.DB, bot *tgbotapi.BotAPI) string {
	novoUsername := tratamentoString(mensagem, "/kick")
	idUsuarioDb := rUser(*db, novoUsername, tabelaUser)
	userAdmin = permCheck(idUsuario)
	if userAdmin == "false" {
		txtdavergonhaArquivo(username, idUsuario, "Usar Comando para ADM")
		return Textos["gracioso"] + username + Textos["mural_da_vergonha"]
	} else if idUsuarioDb == 0 {
		return Textos["user_not_found"]
	} else {
		kickUser(idUsuarioDb, ChatID, bot)
		log.Printf("[-]O usuario " + novoUsername + " de ID:" + strconv.Itoa(idUsuarioDb) + " foi removido pelo admin " + userAdmin + "!")
		return "<b>O usuario " + novoUsername + " de ID:" + strconv.Itoa(idUsuarioDb) + Textos["removido_por"] + userAdmin + "!\nE que não volte mais ;-;!</b>"
	}
}

func banComando(ChatID int64, mensagem string, idUsuario int, username string, db *sql.DB, bot *tgbotapi.BotAPI) string {
	novoUsername := tratamentoString(mensagem, "/ban") //
	idUsuarioDb := rUser(*db, novoUsername, tabelaUser)
	userAdmin = permCheck(idUsuario)
	if userAdmin == "false" {
		txtdavergonhaArquivo(username, idUsuario, " banir um amiguinho do grupo ")
		return Textos["gracioso"] + username + Textos["mural_da_vergonha"]
	} else if idUsuarioDb == 0 {
		return Textos["user_not_found"]
	} else {
		kickUser(idUsuarioDb, ChatID, bot)
		iUser(*db, novoUsername, idUsuarioDb, tabelaBanidos)
		log.Printf("[-]O usuarios " + novoUsername + " de ID:" + strconv.Itoa(idUsuarioDb) + " foi banido pelo admin " + userAdmin + "!")
		return "<b>O usuario " + novoUsername + " de ID:" + strconv.Itoa(idUsuarioDb) + "foi banido do grupo pelo admin " + userAdmin + "\nEsse não vai voltar mais ;-;!</b>"
	}
}

func tdv() string {
	tdv := leitorArquivo(TDV)
	return "<b> MURAL DA VERGONHA </b>\n\n" + tdv + ""
}

// Clear limpa o mural da vergonha
func Clear(mensagem string, idUsuario int, username string) string {
	mensagem = tratamentoString(mensagem, "/clear")
	if permCheck(idUsuario) == "false" {
		txtdavergonhaArquivo(username, idUsuario, " limpar um arquivo ")
		return Textos["te_peguei"]
	}

	err := os.Remove(mensagem)
	if err != nil {
		return Textos["user_not_found"]
	}
	return "<b>Arquivo " + mensagem + Textos["apagado"]

}

func comandos(mensagem string, idUsuario int, username string, ChatID int64, db *sql.DB, bot *tgbotapi.BotAPI, logAPI tgbotapi.APIResponse) string {

	switch mensagem != "" {
	case strings.Contains(mensagem, "/func_regras"):
		return funcRegras(mensagem, bot.Self.FirstName, idUsuario)
	case strings.Contains(mensagem, "/regras"):
		return regrasPrint()
	case strings.Contains(mensagem, "/help"):
		return help()
	case strings.Contains(mensagem, "/admins"):
		return adminsComando(ChatID, bot)
	case strings.Contains(mensagem, "/kick"):
		return kickComando(ChatID, mensagem, idUsuario, username, db, bot)
	case strings.Contains(mensagem, "/txt_da_vergonha"):
		return tdv()
	case strings.Contains(mensagem, "/clear"):
		return Clear(mensagem, idUsuario, username)
	case strings.Contains(mensagem, "/ban"):
		return banComando(ChatID, mensagem, idUsuario, username, db, bot)
	default:
		return "Comando não encontrado,use o /help para saber os comandos!"
	}
}

//FUNÇÕES RELACIONADAS AO SISTEMA
func leitorArquivo(arquivo string) string {
	conteudoArquivo, err := ioutil.ReadFile(arquivo)
	if err != nil {
		logError(err)
		log.Printf("[-]O arquivo " + arquivo + " não pode ser lido!")
		return ""
	}
	return string(conteudoArquivo)
}
func escreveArquivo(arquivo string, conteudo string) {
	os.Remove("imagem.jpg")
	conteudoArquivo := []byte(conteudo)
	err := ioutil.WriteFile(arquivo, conteudoArquivo, 0777)
	logError(err)
}
func tratamentoString(mensagem string, comando string) string {
	mensagem = strings.Replace(mensagem, comando, "", -1)
	mensagem = strings.Replace(mensagem, " ", "", -1)
	if strings.Contains(mensagem, "@") {
		mensagem = strings.Replace(mensagem, "@", "", -1)
	}
	return mensagem
}
func jsonS(decode *tgbotapi.User) string {
	a := decode
	out, err := json.Marshal(a)
	logError(err)
	return string(out)
}
